var backdrop = document.querySelector('.backdrop');

var sideMenu = document.getElementById('sidemenu');

function showMenu(){
    sideMenu.style.left = "0";
    backdrop.classList.remove('closed');
}

backdrop.addEventListener('click', function(){
    sideMenu.style.left = "-380px";
    backdrop.classList.add('closed');
});
